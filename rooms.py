rooms = {

    'Hall': {
        'south': 'Kitchen',
        'east': 'Dining Room'
    },

    'Kitchen': {
        'north': 'Hall',
        'item': 'monster',
    },
    'Dining Room': {
        'west': 'Hall',
        'south': 'Garden',
        'north': "Bedroom",
        'item': 'potion'
    },
    'Bedroom': {
        'south': 'Dining Room',
        'item': 'key'
    },
    'Garden': {
        'north': 'Dining Room'
    }
}
