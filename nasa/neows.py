#!/usr/bin/python3
import requests
import json
import os
import dotenv

dotenv.load_dotenv()

NEOURL = "https://api.nasa.gov/neo/rest/v1/feed?"

def main():
    # with open(os.path.join(os.path.dirname(__file__), "nasa.creds"), "r") as f:
    #     api_key = f.read()
    #     api_key = api_key.strip("\n")
    #     print(api_key)
    api_key = os.environ["API_KEY"]
    nasacreds = "api_key=" + api_key
    
    startdate = input("Start date (yyyy-mm-dd): ")
    if startdate.strip() == "":
        startdate = "start_date=2019-11-11"
    else:
        startdate = "start_date=" + startdate
    
    end_date = input("End date (yyyy-mm-dd): ")
    if end_date.strip() == "":
        end_date = "end_date=2019-11-18"
    else:
        end_date = "end_date=" + end_date

    fullurl = f"{NEOURL}{startdate}&{end_date}&{nasacreds}"

    response = requests.get(fullurl)
    neodata = response.json()

    print(neodata)
if __name__== "__main__":
    main()