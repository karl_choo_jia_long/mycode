#!/usr/bin/python3
import urllib.request
import json
import os

NASAAPI = "https://api.nasa.gov/planetary/apod?"

def main():
    with open(os.path.join(os.path.dirname(__file__), "nasa.creds"), "r") as f:
        api_key = f.read()
        api_key = api_key.strip("\n")
        print(api_key)
    nasacreds = "api_key=" + api_key
    apodurlobj = urllib.request.urlopen(NASAAPI + nasacreds)
    apodread = apodurlobj.read()
    apod = json.loads(apodread.decode("utf-8"))

    print("\n\nConverted Python data")
    print(apod)

    print()

    print(apod["title"] + "\n")

    print(apod["date"] + "\n")

    print(apod["explanation"] + "\n")

    print(apod["url"])

if __name__== "__main__":
    main()