#!/usr/bin/python3
"""tracking the iss using
   api.open-notify.org/astros.json | Alta3 Research"""

import requests
import json
import reverse_geocoder as rg
from datetime import datetime

iss_url = "http://api.open-notify.org/iss-now.json"

try:
    resp = requests.get(iss_url)
    iss_data = resp.json()
    if resp.status_code == 200:

        lon = iss_data['iss_position']['longitude']
        lat = iss_data['iss_position']['latitude']
        print("CURRENT LOCATION OF THE ISS:")
        print(f"Timestamp: {datetime.fromtimestamp(iss_data['timestamp'])}")
        print(f"Lon: {lon}")
        print(f"Lat: {lat}")

        coords_tuple= (lat, lon)
        result = rg.search(coords_tuple, verbose=False)
        coords_data = result[0]

        print(f"City/Country: {coords_data['name']}, {coords_data['cc']}")
    else: 
        print("Status code returned is not 200.")
        print(iss_data)
except requests.RequestException as e:
    print("Error when retrieveing data for ISS.")
    print(e)
except Exception as e:
    print("Error when retrieveing data for ISS.")
    print(e)