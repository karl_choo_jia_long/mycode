#!/usr/bin/env python3
"""Alta3 Research
   Star Wars API HTTP response parsing"""

from pprint import pprint
import requests

URL = "https://swapi.dev/luke/force"      # Comment out this line
URL= "https://swapi.dev/api/people/4/"     # Uncomment this line
film_url = "https://swapi.dev/api/films/1/"
starship_url = "https://swapi.dev/api/starships/13/"

def main():
    """sending GET request, checking response"""

    # SWAPI response is stored in "resp" object
    resp = requests.get(URL)
    resp1 = requests.get(film_url)
    resp2 = requests.get(starship_url)

    # check to see if the status is anything other than what we want, a 200 OK
    if resp.status_code == 200 and resp1.status_code == 200 and resp2.status_code == 200:
        # convert the JSON content of the response into a python dictionary
        people_data = resp.json()
        film_data = resp1.json()
        starship_data = resp2.json()
        # pprint(film_data)
        # print(starship_data)
        print(f"{{{people_data['name']}}} was born in the year {{{people_data['birth_year']}}}. His eyes are now {{{people_data['eye_color']}}} and his hair color is {{{people_data['hair_color']}}}.")
        print(f"He first appeared in the movie {{{film_data['title']}}} and could be found flying around in his {{{starship_data['name']}}}.")
    else:
        print("Error has occured while retrieving the data.")

if __name__ == "__main__":
    main()