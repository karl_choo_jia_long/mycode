#!/usr/bin/python3
"""Alta3 Research - Exploring OpenAPIs with requests"""
# documentation for this API is at
# https://anapioficeandfire.com/Documentation

import requests
import pprint

AOIF_CHAR = "https://www.anapioficeandfire.com/api/characters/"

def compile_api_results(url_list):
    result = []
    for url in url_list:
        resp = requests.get(url)
        resp_dj = resp.json()
        result.append(resp_dj)
    return result

def main():
    ## Ask user for input
    got_charToLookup = input("Pick a number between 1 and 1000 to return info on a GoT character! " )

    ## Send HTTPS GET to the API of ICE and Fire character resource
    gotresp = requests.get(AOIF_CHAR + got_charToLookup)

    ## Decode the response
    got_dj = gotresp.json()
    pprint.pprint(got_dj)

    print("This character belongs to the following houses:")
    for house in compile_api_results(got_dj['allegiances']):
        print(house['name'])
    
    print("This character belongs to the following books:")
    for book in compile_api_results(got_dj['books']):
        print(book['name'])

if __name__ == "__main__":
    main()