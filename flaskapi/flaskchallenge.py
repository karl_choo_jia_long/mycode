#!/usr/bin/python3

from flask import Flask
from flask import request
from flask import redirect
from flask import url_for
from flask import session
from flask import render_template

app = Flask(__name__)

app.secret_key = "alta3_research_is_fun"

groups = [{"hostname": "hostA","ip": "192.168.30.22", "fqdn": "hostA.localdomain"},
          {"hostname": "hostB", "ip": "192.168.30.33", "fqdn": "hostB.localdomain"},
          {"hostname": "hostC", "ip": "192.168.30.44", "fqdn": "hostC.localdomain"}]

@app.route("/", methods= ["GET"])
def index():
    if "username" in session and not session["username"] is None:
        return redirect(url_for("group"))
    return render_template("group_login.html")

@app.route("/login", methods= ["GET", "POST"])
def login():
    if request.method == "GET":
        return redirect(url_for("index"))
    if request.method == "POST":
        session["username"] = request.form.get("username")
        return redirect(url_for("group"))
    

@app.route("/group", methods= ["GET","POST"])
def group():

    if request.method == "POST":
        hostname = request.form.get("hostname")
        ip = request.form.get("ip")
        fqdn = request.form.get("fqdn")
        # create a new dictionary with values, add to groups
        groups.append({"hostname": hostname, "ip": ip, "fqdn": fqdn})

    # print(session)
    # if "username" in session and session["username"] is None:
    #     return redirect(url_for("index"))

    return render_template("group_display.html", groups=groups, username=session["username"])

@app.route("/logout", methods = ["GET"])
def logout():
    session.pop("username", None)
    return redirect(url_for("index"))

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=2224, debug= True)