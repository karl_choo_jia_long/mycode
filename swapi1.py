#!/usr/bin/env python3
"""Alta3 Research
   Star Wars API HTTP response parsing"""

from pprint import pprint
import requests

url = "https://swapi.dev/api/people/four"


def main():
    """sending GET request, checking response"""

    # SWAPI response is stored in "resp" object
    resp = requests.get(url)

    # convert the JSON content of the response into a python dictionary
    vader = resp.json()

    pprint(vader)


if __name__ == "__main__":
    main()
