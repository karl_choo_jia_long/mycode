#!/usr/bin/env python3
"""GitLab Client - OAUth and API study | Alta3 Research"""
# https://docs.gitlab.com/ee/api/projects.html - GitLab API related to repos/projects from here

import json
import os
import requests

def get_token() -> str:
    # Read token in from file, the file is under mycode/oauth/ but has been .gitignored
    with open(os.path.join(os.path.dirname(__file__), "token")) as f:
        token = f.read().rstrip("\n")
        return token

def create_repo(repo_name, token):
    headers = {"PRIVATE-TOKEN": token}
    data = {"name": repo_name, "path": "new_project"}
    response = requests.post("https://gitlab.com/api/v4/projects/", headers=headers, data=data)
    created_data = response.json()

    print(created_data)

def show_repos(user_id, token):
    headers = {"PRIVATE-TOKEN": token, "Content-Type": "application/json"}
    response = requests.get(f"https://gitlab.com/api/v4/users/{user_id}/projects/", headers=headers)
    repos_data = response.json()
    print(json.dumps(repos_data, indent=2))

def main():
    user_id = "8069048" # Your gitlab user id, can creck from profile page
    tkn = get_token()
    create_repo("learning_oauth", tkn) # Uncomment to create repo
    show_repos(user_id, tkn)

    
if __name__ == "__main__":
    main()