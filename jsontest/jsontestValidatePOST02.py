#!/usr/bin/python3

import requests
import datetime
import os

# define the URL we want to use
TIMEURL = "http://date.jsontest.com"
IPURL = "http://ip.jsontest.com"
VALIDURL = "http://validate.jsontest.com/"

def main():
    datetime_data = {'date': '', 'milliseconds_since_epoch': 0, 'time': ''}
    try:
        response = requests.get(TIMEURL)
        datetime_data = response.json()
        print(datetime_data)
    except requests.RequestException as e:
        print(e)

    ip_data = {"ip": ""}
    try:
        response = requests.get(IPURL)
        ip_data = response.json()
        print(ip_data)
    except requests.RequestException as e:
        print(e)

    filepath = os.path.join(os.path.dirname(__file__),"myservers.txt")
    servers = []
    with open(filepath) as f:
        servers = f.readlines()
        print(servers)
    
    mydata = {"json": f"time: {datetime_data['date']} {datetime_data['time']}, ip: '{ip_data['ip']}', mysvrs: {servers}"}
    print(mydata)
    response = requests.post(VALIDURL, data=mydata)
    respjson = response.json()
    print(respjson)
    print(f"Is your JSON valid? {respjson['validate']}")

if __name__ == "__main__":
    main()