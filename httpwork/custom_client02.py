#!/usr/bin/env python3
import http.client
import os

def main():
    ## think of this as setting up the connection
    conn = http.client.HTTPConnection("localhost", 9021)

    print("Menu")
    print("-"*30)
    print("(1) Test connection")
    print("(2) Get request")
    user_choice = input()
    ## Send an HTTP request and store the HTTP response
    ##    from our webserver
    if user_choice == "1":
        conn.request('HEAD', '/')

        ## Returns just the response that has been associated with
        ##    the **conn** object.
        res = conn.getresponse()
    
        ## response status and the reason to the screen.
        print(res.status, res.reason)
    elif user_choice == "2":
        ## this time we'll issue GET
        conn.request('GET', '/')
        
        ## res is equal to the response associated with conn
        res = conn.getresponse()
        
        ## print the response status code and reason
        print(res.status, res.reason)
        
        ## page_data is all of the data associated with res 
        page_data = res.read().decode("utf-8")

        ## this will point out all of the data associated with res
        print(page_data)

        with open(os.path.join(os.path.dirname(__file__),"http_readin.txt"), "w") as f:
            f.write(page_data)
    else:
        print("Invalid choice")

if __name__ == "__main__":
    main()
