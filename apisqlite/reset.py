#!/usr/bin/env python3

import sqlite3
import os
dbpath = os.path.join(os.path.dirname(__file__),'mymovie.db')
conn = sqlite3.connect(dbpath)
print("Opened database successfully")
cursor = conn.execute("DROP TABLE MOVIES")

print("Operation done successfully")
conn.close()
