#!/usr/bin/env python3

import sqlite3
import os
dbpath = os.path.join(os.path.dirname(__file__),'mymovie.db')
conn = sqlite3.connect(dbpath)
print("Opened database successfully")
cursor = conn.execute("SELECT * from MOVIES")
for row in cursor:
    print(row)

print("Operation done successfully")
conn.close()
